import WebSocket from 'ws';
import log from './utils/log';
import Socket from '../common/utils/socket';
import Room from '../common/utils/room';
import constants from '../common/constants';
import getIp from './utils/get-ip';

const WS_SIZE_LIMIT = process.env.WS_SIZE_LIMIT || 1e8;
const SOCKET_ALIVE_PONG_TIMEOUT = 60 * 1000; // 60 seconds
const RECHECK_ALIVE_SOCKETS_INTERVAL = 40 * 1000; // 40 seconds
const CONNECTION_EXPIRY_TIME = 5 * 60 * 1000; // 5 minutes in milliseconds

const wss = new WebSocket.Server({ noServer: true });
const rooms = {};

/**
 * Checks if a user's socket is active by sending a PING and
 * anticipating a PONG response within SOCKET_ALIVE_PONG_TIMEOUT
 * seconds from the user.
 *
 * @param {string} room Room name
 * @param {Socket} user User socket to check if it is active
 * @returns {Promise} Promise that resolves if user is active otherwise rejects
 */
const isUserAlive = (room, user) =>
  new Promise((resolve, reject) => {
    let timeoutId = setTimeout(() => {
      timeoutId = null;
      room.removeSocket(user);
      reject();
    }, SOCKET_ALIVE_PONG_TIMEOUT);

    user.socket.on('pong', () => {
      if (timeoutId === null) return;

      clearTimeout(timeoutId);
      resolve();
    });

    log(
      `Checking if ${user.name} is alive. Someone with same name is trying to join`
    );
    user.socket.ping();
  });

const handleConnection = (ws, request) => {
  ws.isAlive = true;
  ws.lastPingTimestamp = Date.now();

  const ip = getIp(request);

  const socket = new Socket(ws, ip);
  let room;

  socket.listen(constants.JOIN, async (data) => {
    let { roomName, name, peerId } = data;
    socket.name = name;
    socket.peerId = peerId;
    roomName = roomName || socket.ip;
    if (!roomName) {
      /**
       * If the room name is falsy, then the socket possibly disconnected while joining a local room.
       * Close the socket from server end and terminate the flow.
       */
      socket.close(1000);
      return;
    }

    room = rooms[roomName];

    if (room) {
      // 1. Check if there's some user with the same name in the room
      const user = room.getSocketFromName(socket.name);
      if (user) {
        try {
          // 2. If a user with the same name exists, check if they are still active
          await isUserAlive(room, user);
          // 3. If they are active, close the connection of the user trying to join
          socket.close(1000, constants.ERR_SAME_NAME);
          return;
        } catch {
          // 4. If the user is not active, they are removed from the room
          // Now the user trying to join is let in
        }
      }
    }

    /**
     * 5. Check if the room still exists, as after removing any potential inactive users
     *    with the same name might have caused the room to become empty and hence deleted
     */
    room = rooms[roomName];
    if (!room) {
      // 6. Create room if it does not exist
      rooms[roomName] = new Room(roomName);
      room = rooms[roomName];
    }

    // 7. Add the user to the room
    log(`${name} has joined ${roomName}`);

    room.addSocket(socket);
    room.broadcast(constants.USER_JOIN, room.socketsData);
  });

  socket.on('close', (data) => {
    if (data.reason === constants.ERR_SAME_NAME) return;
    if (!room) return;

    room.removeSocket(socket);
  });

  socket.on('pong', () => {
    socket.isAlive = true;
  });

  socket.listen(constants.FILE_INIT, (data) => {
    // TODO: Prevent init from multiple sockets if a sender is already there
    // TODO: Improve error messaging via sockets
    if (data.size > WS_SIZE_LIMIT) return;

    if (data.end) {
      log(`File transfer just finished!`);
    } else {
      log(`${socket.name} has initiated file transfer`);
    }

    room.sender = socket.name;
    room.broadcast(constants.FILE_INIT, data, [socket.name]);
  });

  socket.listen(constants.FILE_STATUS, (data) => {
    const sender = room.senderSocket;
    // TODO: Sender is not there but some file is getting transferred!
    if (!sender) return;

    sender.send(constants.FILE_STATUS, data);
  });

  socket.listen(constants.CHUNK, (data) => {
    room.broadcast(constants.CHUNK, data, [room.sender]);
  });

  socket.listen(constants.FILE_TORRENT, (data) => {
    room.broadcast(constants.FILE_TORRENT, data, [socket.name]);
  });
};

// Handle new connections
wss.on('connection', handleConnection);

// Variables to track page visibility and app focus
let isPageVisible = true; // Initially assume the page is visible
let isAppFocused = true; // Initially assume the app is focused

// Check if the environment supports document (e.g., browser environment)
if (typeof document !== 'undefined') {
  // Use the Page Visibility API to track tab visibility changes
  document.addEventListener('visibilitychange', () => {
    isPageVisible = !document.hidden;

    wss.clients.forEach((ws) => {
      if (!isPageVisible) {
        // Tab is not visible, pause the WebSocket connection
        ws.pause();
      } else {
        // Tab is visible, resume the WebSocket connection
        ws.resume();
      }
    });
  });

  // Use the Focus and Blur events to track app focus changes
  window.addEventListener('focus', () => {
    isAppFocused = true;

    wss.clients.forEach((ws) => {
      if (isPageVisible && ws.isAlive === false) {
        // Tab is visible and connection is not alive, close the WebSocket connection
        ws.terminate();
      } else if (isPageVisible) {
        // Tab is visible, resume the WebSocket connection
        ws.resume();
      }
    });
  });

  window.addEventListener('blur', () => {
    isAppFocused = false;

    wss.clients.forEach((ws) => {
      // Tab is not visible, pause the WebSocket connection
      ws.pause();
    });
  });

  // Use the BeforeUnload event to close WebSocket connections gracefully
  window.addEventListener('beforeunload', () => {
    wss.clients.forEach((ws) => {
      ws.close(1000, 'User left the page');
    });
  });

  // Check for connection expiry and terminate old connections
  setInterval(() => {
    const currentTime = Date.now();
    wss.clients.forEach((ws) => {
      if (ws.isAlive && currentTime - ws.lastPingTimestamp > CONNECTION_EXPIRY_TIME) {
        ws.terminate();
      }
    });
  }, RECHECK_ALIVE_SOCKETS_INTERVAL);
}

// Interval for checking alive sockets and pinging clients
const interval = setInterval(() => {
  log('Checking alive sockets');
  wss.clients.forEach((ws) => {
    if (ws.isAlive === false) return ws.terminate();

    ws.isAlive = false;
    ws.ping();
  });
}, RECHECK_ALIVE_SOCKETS_INTERVAL);

// Handle server close event
wss.on('close', () => {
  clearInterval(interval);
});

// Export WebSocket Server and rooms
export { wss, rooms };
